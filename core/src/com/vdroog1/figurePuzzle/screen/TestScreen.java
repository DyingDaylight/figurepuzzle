package com.vdroog1.figurePuzzle.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.vdroog1.figurePuzzle.Constants;
import com.vdroog1.figurePuzzle.PuzzleGame;
import com.vdroog1.figurePuzzle.Util;
import com.vdroog1.figurePuzzle.data.Shape;
import com.vdroog1.figurePuzzle.model.Figure;
import com.vdroog1.figurePuzzle.processor.FigureProcessor;

/**
 * Created by kettricken on 01.03.2015.
 */
public class TestScreen extends ScreenAdapter {

    private PuzzleGame game;
    private OrthographicCamera camera;
    private Viewport viewport;

    FigureProcessor figureProcessor;

    ShapeRenderer shapeRenderer = new ShapeRenderer();

    Sprite sprite;

    PolygonSpriteBatch polygonSpriteBatch;

    public TestScreen(PuzzleGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(true, Constants.WORLD_WIDTH_M, Constants.WORLD_HEIGHT_M);
        viewport = new StretchViewport(Constants.WORLD_WIDTH_M, Constants.WORLD_WIDTH_M, camera);

        Gdx.input.setInputProcessor(new InputListener());

        figureProcessor = new FigureProcessor();
        figureProcessor.initLevel(Shape.SQUARE, 100);

        polygonSpriteBatch = new PolygonSpriteBatch();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);

        figureProcessor.drawDebug(shapeRenderer);
        int end = figureProcessor.getFigures().size;

        polygonSpriteBatch.setProjectionMatrix(camera.combined);
        polygonSpriteBatch.begin();
        for (int i = 0; i < end; i++) {
            Figure figure = figureProcessor.getFigures().get(i);
            figure.getPolygonSprite().setPosition(figure.getPosition().x, figure.getPosition().y);
            figure.getPolygonSprite().draw(polygonSpriteBatch);
            polygonSpriteBatch.flush();
        }
        polygonSpriteBatch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (int i = 0; i < end; i++) {
            Figure figure = figureProcessor.getFigures().get(i);
            shapeRenderer.setColor(1,0.5f,0,1);
            shapeRenderer.polygon(Util.getFloatArray(figure.getVertices()));
            shapeRenderer.setColor(1,1,0,1);
            shapeRenderer.circle(figure.getPosition().x, figure.getPosition().y, 0.3f);
        }
        shapeRenderer.end();
    }

    private class InputListener extends InputAdapter {

        @Override
        public boolean keyDown(int keycode) {
            switch (keycode){
                case Input.Keys.Q:
                    figureProcessor.switchDrawShape();
                    break;
                case Input.Keys.W:
                    figureProcessor.deleteRandomLines();
                    break;
                case Input.Keys.E:
                    figureProcessor.applyShapeMask();
                    break;
                case Input.Keys.R:
                    figureProcessor.divideOnFigures();
                    break;
                case Input.Keys.LEFT:
                    figureProcessor.decrementIndex();
                    break;
                case Input.Keys.RIGHT:
                    figureProcessor.incrementIndex();
                    break;
            }
            return super.keyDown(keycode);
        }
    }

}
