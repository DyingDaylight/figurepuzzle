package com.vdroog1.figurePuzzle.screen;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.vdroog1.figurePuzzle.Constants;
import com.vdroog1.figurePuzzle.PuzzleGame;
import com.vdroog1.figurePuzzle.Util;
import com.vdroog1.figurePuzzle.data.Shape;
import com.vdroog1.figurePuzzle.model.Figure;
import com.vdroog1.figurePuzzle.processor.BodyProcessor;
import com.vdroog1.figurePuzzle.processor.DirectionGestureDetector;
import com.vdroog1.figurePuzzle.processor.DrawingProcessor;
import com.vdroog1.figurePuzzle.processor.FigureProcessor;

/**
 * Created by kettricken on 08.02.2015.
 */
public class GameScreen extends ScreenAdapter {

    private static final float HANGING_TIME = 30f;
    private static final float SHUFFLING_HANGING_TIME = 0.6f;
    private static final float CAMERA_ROTATION_SPEED = 0.05f;

    private PuzzleGame game;
    private OrthographicCamera camera;
    private Viewport viewport;

    private World world;
    private float accumulator = 0;

    private boolean isExpanding = false;
    private boolean isUniting = false;
    private boolean isRotating = false;
    private boolean isShuffling = false;
    private boolean isRandomizing = false;

    private boolean isGroupingMode = false;

    private FigureProcessor figureProcessor;

    private Box2DDebugRenderer box2DDebugRenderer;
    private DrawingProcessor drawingProcessor;
    private BodyProcessor bodyProcessor;

    private int rotationDirection = 1;
    private float hangingTime = 0;
    private float cameraAngle = 360;
    private float cameraZoom = 1;

    public GameScreen(PuzzleGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(true, Constants.WORLD_WIDTH_M, Constants.WORLD_HEIGHT_M);
        viewport = new StretchViewport(Constants.WORLD_WIDTH_M, Constants.WORLD_HEIGHT_M, camera);

        Box2D.init();
        world = new World(new Vector2(0, 0), true);

        initInputListeners();

        box2DDebugRenderer = new Box2DDebugRenderer();
        drawingProcessor = new DrawingProcessor(camera, this.game.getBatch());
        bodyProcessor = new BodyProcessor(world, this);

        figureProcessor = new FigureProcessor();
    }

    @Override
    public void show() {
        startNewLevel(Shape.SQUARE, 100);
        shuffle();
    }

    @Override
    public void dispose() {
        figureProcessor.dispose();
        world.dispose();
        drawingProcessor.dispose();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (isShuffling &&
                (isRandomizing || isUniting) &&
                hangingTime < SHUFFLING_HANGING_TIME)
            hangingTime += delta;
        else {
            int stoppedBodiesCount = bodyProcessor.updateBodies(delta, rotationDirection);
            updateStat(delta, stoppedBodiesCount);
        }
        updateCamera(delta);
        bodyProcessor.cleanMouseJoints();

        camera.update();
        doPhysicsStep(delta);

        box2DDebugRenderer.render(world, camera.combined);

        drawingProcessor.render(delta, bodyProcessor.getBodies());
    }

    private void doPhysicsStep(float delta) {
        float frameTime = Math.min(delta, 0.25f);
        accumulator += frameTime;
        while (accumulator >= Constants.TIME_STEP) {
            world.step(Constants.TIME_STEP, Constants.VELOCITY_ITERATIONS,
                    Constants.POSITION_ITERATIONS);
            accumulator -= Constants.TIME_STEP;
        }
    }

    private void initInputListeners() {
        InputMultiplexer inputMultiplexer = new InputMultiplexer(new GestureDetector(new TapListener()));
        inputMultiplexer.addProcessor(new DirectionGestureDetector(new DirectionGestureDetector.DirectionGestureListener(new RotationDirectionListener())));
        inputMultiplexer.addProcessor(new KeysListener());
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    private void startNewLevel(Shape shape, int seed) {
        figureProcessor.initLevel(shape, seed);

        bodyProcessor.createWalls();
        for (int i = 0; i < figureProcessor.getFigures().size; i++) {
            Figure figure = figureProcessor.getFigures().get(i);
            bodyProcessor.createBody(figure);
        }
    }

    private void shuffle() {
        //TODO: is shuffling iterative?
        if (bodyProcessor.shuffle()) {

            float desiredWorldSize = bodyProcessor.getDesiredWorldSize();
            float scaleX = desiredWorldSize / Constants.WORLD_WIDTH_M;
            float scaleY = desiredWorldSize / Constants.WORLD_HEIGHT_M;
            cameraZoom = Math.max(1, Math.max(scaleX, scaleY));

            hangingTime = 0;
            setExpanding();
            isShuffling = true;
        } else {
            hangingTime = 0;
            rotationDirection = 1;
            resetGameStats();
        }
    }

    private void restart() {

        resetGameStats();

        bodyProcessor.resetBodies();

        resetCamera();
        resetParameters();

        shuffle();
    }

    private void resetParameters() {
        cameraAngle = 360;
        cameraZoom = 1;
        rotationDirection = 1;
        hangingTime = 0;
    }

    private void resetCamera() {
        camera.rotate(360 - cameraAngle);
        camera.position.x = Constants.WORLD_WIDTH_M / 2;
        camera.position.y = Constants.WORLD_HEIGHT_M / 2;
        camera.zoom = 1;
    }

    private void resetGameStats() {
        isExpanding = false;
        isRotating = false;
        isUniting = false;
        isShuffling = false;
        isRandomizing = false;
        isGroupingMode = false;
    }

    private void updateCamera(float delta) {
        //TODO: set camera for shuffling
        if (!isShuffling) {
            rotateCamera();
            moveCamera();
        }
        scaleCamera(delta);
    }

    private void scaleCamera(float delta) {
        if (!isUniting && cameraZoom > 1 && camera.zoom < cameraZoom) {
            float dif = cameraZoom - camera.zoom;
            camera.zoom += dif * delta;
        } else if (!isExpanding && !isRotating && !isRandomizing && camera.zoom > 1) {
            float dif = camera.zoom - 1;
            camera.zoom = Math.max(camera.zoom - dif * delta, 1);
            cameraZoom = 1;
        }
    }

    private void rotateCamera() {
        if (isRotating) {
            float playerAngle = Util.radiansToDegrees(bodyProcessor.getClickedBody().getAngle());
            float cameraAngleDelta = playerAngle - cameraAngle;
            cameraAngle = playerAngle;

            camera.rotate(cameraAngleDelta);
        }
    }

    private void moveCamera() {
        if (isExpanding || isRotating) {
            if (bodyProcessor.getClickedBody() == null)
                return;

            Vector2 cameraDestination = bodyProcessor.getClickedBody().getPosition();

            float diff = (float) Math.sqrt(Math.pow(camera.position.x - cameraDestination.x, 2)
                    + Math.pow(camera.position.y - cameraDestination.y, 2));

            if (diff > CAMERA_ROTATION_SPEED) {
                camera.position.x = camera.position.x + CAMERA_ROTATION_SPEED / diff * (cameraDestination.x - camera.position.x);
                camera.position.y = camera.position.y + CAMERA_ROTATION_SPEED / diff * (cameraDestination.y - camera.position.y);
            } else {
                camera.position.x = bodyProcessor.getClickedBody().getPosition().x;
                camera.position.y = bodyProcessor.getClickedBody().getPosition().y;
                camera.position.z = 0;
            }
        }
    }

    private void setExpanding() {
        isRandomizing = false;
        isExpanding = true;
        isRotating = false;
        isUniting = false;
    }

    private void setRotating() {
        isRandomizing = false;
        isExpanding = false;
        isRotating = true;
        isUniting = false;
    }

    private void setUniting() {
        isRandomizing = false;
        isExpanding = false;
        isRotating = false;
        isUniting = true;
    }

    private void setRandomizing() {
        isRandomizing = true;
        isExpanding = false;
        isRotating = false;
        isUniting = false;
    }

    private void updateStat(float delta, int stoppedBodiesCount) {
        if (isExpanding && stoppedBodiesCount == bodyProcessor.getMovingBodiesCount() && isShuffling
                && !isRandomizing) {

            bodyProcessor.randomize();
            setRandomizing();
            return;
        }

        if (isRandomizing && stoppedBodiesCount == bodyProcessor.getMovingBodiesCount()) {
            hangingTime = 0;
            bodyProcessor.restoreMask();
            setUniting();
        }

        if (isExpanding && stoppedBodiesCount == bodyProcessor.getMovingBodiesCount()) {
            setRotating();
        }

        if (isRotating) {
            hangingTime += delta;
            if (hangingTime >= HANGING_TIME)
                setUniting();
        }

        if (bodyProcessor.getMovingBodiesCount() == 0 && isUniting) {
            if (isShuffling) {
                resetGameStats();
                //shuffle();
            } else {
                figureProcessor.isCompleted(bodyProcessor.getBodies());
                resetGameStats();
            }
        }
    }

    public boolean isShuffling() {
        return isShuffling;
    }

    public boolean isExpanding() {
        return isExpanding;
    }

    public boolean isRandomizing() {
        return isRandomizing;
    }

    public boolean isUniting() {
        return isUniting;
    }

    public boolean isRotating() {
        return isRotating;
    }

    private class KeysListener extends InputAdapter {

        @Override
        public boolean keyDown(int keycode) {
            switch (keycode){
                case Input.Keys.R:
                    restart();
                    break;
                case Input.Keys.G:
                    isGroupingMode = !isGroupingMode;
                    break;
            }
            return super.keyDown(keycode);
        }
    }

    private class TapListener extends GestureDetector.GestureAdapter {

        Vector3 touchPoint = new Vector3();

        @Override
        public boolean tap(float screenX, float screenY, int count, int button) {
            touchPoint.set(screenX, screenY, 0);
            camera.unproject(touchPoint);
            if (isGroupingMode) {
                bodyProcessor.groupBodies(touchPoint);
                return false;
            }

            if (isExpanding || isRotating) {
                setUniting();
                return false;
            }

            if (bodyProcessor.tapBody(touchPoint)) {
                cameraAngle = Util.radiansToDegrees(bodyProcessor.getClickedBody().getAngle());
                float desiredWorldSize = bodyProcessor.getDesiredWorldSize();
                float scaleX = desiredWorldSize / Constants.WORLD_WIDTH_M;
                float scaleY = desiredWorldSize / Constants.WORLD_HEIGHT_M;
                cameraZoom = Math.max(1, Math.max(scaleX, scaleY));
                hangingTime = 0;
                setExpanding();
            }
            return false;
        }
    }

    private class RotationDirectionListener implements DirectionGestureDetector.DirectionListener {

        @Override
        public void clockwise() {
            if (isRotating)
                rotationDirection = -1;
        }

        @Override
        public void counterClockwise() {
            if (isRotating)
                rotationDirection = 1;
        }
    }
}
