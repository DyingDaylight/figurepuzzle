package com.vdroog1.figurePuzzle.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.utils.Array;
import com.vdroog1.figurePuzzle.Util;
import net.dermetfan.gdx.math.GeometryUtils;

/**
 * Created by kettricken on 01.03.2015.
 */
public class Figure {

    private int[][] matrix;

    private Polygon polygon;
    private Polygon[] convexPolygons;

    private Array<Vector2> verticesBox2D = new Array<Vector2>();
    private Array<Vector2> verticesSprite = new Array<Vector2>();
    private Array<Vector2> vertices = new Array<Vector2>();
    private Array<Boolean> indexMask = new Array<Boolean>();
    private Array<Boolean> tmpIndexMask = new  Array<Boolean> ();
    private Array<Integer> indexes = new Array<Integer>();

    private final Vector2 centerBox2D = new Vector2();
    private final Vector2 position = new Vector2();
    private final Vector2 centerSprite = new Vector2();

    private PolygonSprite polygonSprite;

    private float width;
    private float height;

    private MouseJoint mouseJoint;
    private final Vector2 destination = new Vector2();
    private float desiredDistance;
    private float speed;

    private Array<Body> jointBodies = new Array<Body>();

    // --- getters and setters ---
    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public Polygon[] getConvexPolygons() {
        return convexPolygons;
    }

    public Array<Vector2> getVertices() {
        return vertices;
    }

    public Array<Vector2> getVerticesBox2D() {
        return verticesBox2D;
    }

    public Array<Vector2> getVerticesSprite() {
        return verticesSprite;
    }

    public Array<Integer> getIndexes() {
        return indexes;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getCenterBox2D() {
        return centerBox2D;
    }

    public Vector2 getCenterSprite() {
        return centerSprite;
    }

    public void setPolygonSprite(PolygonSprite texture) {
        this.polygonSprite = texture;
        polygonSprite.setOrigin(centerSprite.x, centerSprite.y);
    }

    public PolygonSprite getPolygonSprite() {
        return polygonSprite;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setMouseJoint(MouseJoint mouseJoint) {
        this.mouseJoint = mouseJoint;
    }

    public MouseJoint getMouseJoint() {
        return mouseJoint;
    }

    public void setDesiredDistance(float desiredDistance) {
        this.desiredDistance = desiredDistance;
    }

    public float getDesiredDistance() {
        return desiredDistance;
    }

    public Vector2 getDestination() {
        return destination;
    }

    public void setDestinationPoint(float x, float y) {
        destination.set(x, y);
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    // --- methods ---
    public void addSideToCell(int column, int row, int side) {
        if (matrix == null)
            throw new IllegalAccessError("addSideToCell: Matrix has not been initialized");

        matrix[row][column] = matrix[row][column] + side;
    }

    public void addVertex(Vector2 newVertex) {
        if (vertices.size < 2) {
            vertices.add(newVertex);
            return;
        }

        int lastIndex = vertices.size - 1;
        int beforeLastIndex = vertices.size - 2;

        Vector2 lastVertex = vertices.get(lastIndex);
        Vector2 beforeLastVertex = vertices.get(beforeLastIndex);

        if ((lastVertex.x == newVertex.x && beforeLastVertex.x == newVertex.x) ||
                (lastVertex.y == newVertex.y && beforeLastVertex.y == newVertex.y)) {
            vertices.removeIndex(lastIndex);
        }

        if (!(newVertex.x == vertices.get(0).x && newVertex.y == vertices.get(0).y)) {
            vertices.add(newVertex);
        }
    }

    public void addToIndexMask(boolean b) {
        indexMask.add(b);
    }

    public void addToTmpIndexMask(int position) {
        tmpIndexMask.set(position, true);
    }

    public void iniFigureParameters() {
        findCenterBox2D();
        findPosition();
        findCenterSprite();
        findBox2DVertices();
        findSpriteVertices();
        findSize();
        findIndices();
        findConvexPolygons();
        findPolygon();
    }

    public void reset() {
        clearMask();
        speed = 0;
        desiredDistance = 0;
    }

    public void clearMask() {
        tmpIndexMask.clear();
        for (int i = 0; i < vertices.size; i++) {
            tmpIndexMask.add(false);
        }
    }

    public float move(Vector2 currentPosition, Vector2 destination, float speed) {
        if (mouseJoint == null)
            return  0;

        float diff = (float) Math.sqrt(Math.pow(currentPosition.x - destination.x, 2)
                + Math.pow(currentPosition.y - destination.y, 2));

        if (diff <= 0.5)
            return diff;

        if (diff < speed)
            speed = diff;

        currentPosition.x = currentPosition.x + speed / diff * (destination.x - currentPosition.x);
        currentPosition.y = currentPosition.y + speed / diff * (destination.y - currentPosition.y);
        mouseJoint.setTarget(currentPosition);
        return diff;
    }

    public boolean isMaskCompleted() {
        for (int i = 0; i < indexMask.size; i ++) {
            if (indexMask.get(i) != tmpIndexMask.get(i))
                return false;
        }
        return true;
    }

    private void findCenterBox2D() {
        if (vertices.size == 0)
            throw new IllegalAccessError("Vertices are not counted yet");

        float x =0, y =0;

        for (Vector2 vertex : vertices) {
            x += vertex.x;
            y += vertex.y;
        }

        centerBox2D.x = x / vertices.size;
        centerBox2D.y = y / vertices.size;
    }

    private void findPosition() {
        if (vertices.size == 0)
            throw new IllegalAccessError("Vertices are not counted yet");

        float minX = Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;

        for (Vector2 vertex : vertices) {
            if (vertex.x < minX)
                minX = vertex.x;
            if (vertex.y < minY)
                minY = vertex.y;
        }
        position.set(minX, minY);
    }

    public void findCenterSprite() {
        centerSprite.set(centerBox2D.x - this.position.x, centerBox2D.y - this.position.y);
    }

    private void findBox2DVertices() {
        if (vertices.size == 0)
            throw new IllegalAccessError("Vertices are not counted yet");

        for (Vector2 vector2 : vertices) {
            verticesBox2D.add(new Vector2(vector2.x - centerBox2D.x, vector2.y - centerBox2D.y));
        }
    }

    private void findSpriteVertices() {
        if (vertices.size == 0)
            throw new IllegalAccessError("Vertices are not counted yet");

        for (Vector2 vector2 : vertices) {
            verticesSprite.add(new Vector2(vector2.x - position.x, vector2.y - position.y));
        }
    }

    private void findSize() {
        if (verticesSprite.size == 0)
            throw new IllegalAccessError("findSize: verticesSprite are not counted yet");

        width = 0;
        height = 0;

        for (Vector2 point : verticesSprite) {
            if (point.x > width)
                width = point.x;
            if (point.y > height)
                height = point.y;
        }
    }

    private void findIndices() {
        for (Vector2 vertex : vertices) {
            indexes.add(getVertexIndex(vertex.x, vertex.y));
        }
        clearMask();
    }

    private int getVertexIndex(float x, float y) {
        if (matrix == null)
            throw new IllegalAccessError("Matrix is empty. Fill the matrix first");

        int matrixDoubledWidth = matrix.length * 2;
        return  (int) (y * 2 * matrixDoubledWidth + x * 2);
    }

    private void findConvexPolygons() {
        Polygon tempPolygon = new Polygon(Util.getFloatArray(verticesBox2D));
        boolean isConvex = GeometryUtils.isConvex(tempPolygon);
        if (!isConvex)
            convexPolygons = GeometryUtils.decompose(tempPolygon);
        else
            convexPolygons = new Polygon[]{tempPolygon};
    }

    private void findPolygon() {
        polygon = new Polygon(Util.getFloatArray(verticesSprite));
        polygon.setOrigin(centerSprite.x, centerSprite.y);
    }

    public void addJointBody(Body body) {
        jointBodies.add(body);
    }

    public Array<Body> getJointBodies() {
        return jointBodies;
    }
}
