package com.vdroog1.figurePuzzle.processor;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.vdroog1.figurePuzzle.Util;
import com.vdroog1.figurePuzzle.model.Figure;

/**
 * Created by kettricken on 13.03.2015.
 */
public class DrawingProcessor implements Disposable{

    private ShapeRenderer shapeRenderer;
    private PolygonSpriteBatch polygonSpriteBatch;
    private SpriteBatch batch;

    private OrthographicCamera camera;

    public DrawingProcessor(OrthographicCamera camera, SpriteBatch batch) {
        shapeRenderer = new ShapeRenderer();
        polygonSpriteBatch = new PolygonSpriteBatch();
        this.batch = batch;

        this.camera = camera;
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
        polygonSpriteBatch.dispose();
    }

    public void render(float delta, Array<Body> bodies) {
        camera.update();
        polygonSpriteBatch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
        batch.setProjectionMatrix(camera.combined);

        polygonSpriteBatch.begin();
        drawGradientPolygons(bodies);
        polygonSpriteBatch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        drawCenters(bodies);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        drawBorders(bodies);
        shapeRenderer.end();
    }

    private void drawBorders(Array<Body> bodies) {
        for (Body body : bodies) {
            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            figure.getPolygon().setPosition(figure.getPolygonSprite().getX(), figure.getPolygonSprite().getY());
            figure.getPolygon().setRotation(figure.getPolygonSprite().getRotation());

            shapeRenderer.setColor(1, 0.5f,0,1);
            shapeRenderer.polygon(figure.getPolygon().getTransformedVertices());
        }
    }

    private void drawCenters(Array<Body> bodies) {
        for (Body body : bodies) {
            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            shapeRenderer.setColor(1, 1, 0, 1);
            shapeRenderer.circle(body.getPosition().x,
                    body.getPosition().y, 0.3f);

        }
    }

    private void drawGradientPolygons(Array<Body> bodies) {
        for (Body body : bodies) {
            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            figure.getPolygonSprite().setPosition(body.getPosition().x - figure.getCenterSprite().x,
                    body.getPosition().y - figure.getCenterSprite().y);
            figure.getPolygonSprite().setRotation(Util.radiansToDegrees(body.getAngle()));
            figure.getPolygonSprite().draw(polygonSpriteBatch);

            polygonSpriteBatch.flush();
        }
    }
}
