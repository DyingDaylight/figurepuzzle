package com.vdroog1.figurePuzzle.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ShortArray;
import com.vdroog1.figurePuzzle.Constants;
import com.vdroog1.figurePuzzle.Util;
import com.vdroog1.figurePuzzle.data.Shape;
import com.vdroog1.figurePuzzle.data.Sides;
import com.vdroog1.figurePuzzle.model.Figure;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by k8n on 01.03.2015.
 * Class to create figures
 */
public class FigureProcessor implements Disposable {

    private int size = 5;
    private float halfSize = size / 2f;

    private int seed = 100;

    private float offsetX = 0;
    private float offsetY = 0;

    private int[][] field;
    private int[][] mainFigure;
    private Shape shape;

    boolean isShapeDrawing = true;
    boolean isMaskApplied = false;
    boolean isPolygonDrawing = false;
    int polygonIndex = 0;

    private Array<Figure> figureList = new Array<Figure>();

    Texture texture;

    public FigureProcessor() {
    }

    public void initLevel(Shape shape, int seed) {
        long start = System.currentTimeMillis();

        this.seed = seed;

        initShape(shape);
        initField();
        initFigure();

        calculateOffset();
        createTexture();

        deleteRandomLines();
        applyShapeMask();
        divideOnFigures();

        long time = System.currentTimeMillis() - start;
        Gdx.app.log("Test", "initLevel time " + time);
    }

    public Array<Figure> getFigures() {
        return figureList;
    }

    public float getOffsetX() {
        return offsetX;
    }

    public float getOffsetY() {
        return offsetY;
    }

    public void isCompleted(Array<Body> bodies) {
        int figuresOnPlace = 0;
        for (Body body : bodies) {
            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            figure.clearMask();

            Array<Vector2> vertices = Util.moveVector2Array(figure.getVerticesBox2D(), body.getPosition());
            for (int i = 0; i < vertices.size; i++) {
                final Vector2 vertex = vertices.get(i);
                if (findNeighborVertexes(bodies, body, figure.getIndexes().get(i), vertex))
                    figure.addToTmpIndexMask(i);
            }

            if (figure.isMaskCompleted()) {
                figuresOnPlace++;
            }
        }
        Gdx.app.log("Test", String.format("Result is %d out of %d", figuresOnPlace, bodies.size));
        if (figuresOnPlace == bodies.size) {
            Gdx.app.log("Test", "!!! VICTORY !!!");
        }
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    private void initShape(Shape shape) {
        this.shape = shape;
    }

    private void initField() {
        if (shape == null)
            throw new IllegalArgumentException("initField: Shape has not been chosen!");

        field = new int[shape.height][shape.width];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                field[i][j] = 255;
            }
        }
    }

    private void initFigure() {
        if (shape == null)
            throw new IllegalArgumentException("initFigure: Shape has not been chosen!");

        mainFigure = new int[shape.height][shape.width];
    }

    private void calculateOffset() {
        if (field == null)
            throw new IllegalArgumentException("calculateOffset: Field has not been initialized!");

        offsetX = Constants.WORLD_WIDTH_M / 2 - field[0].length * size / 2;
        offsetY = Constants.WORLD_HEIGHT_M / 2 - field.length * size / 2;
    }

    private void createTexture() {
        Color topLeft = new Color(1, 0, 0, 1);
        Color topRight = new Color(0, 0, 1, 1);
        Color bottomRight = new Color(0, 1, 0, 1);
        Color bottomLeft = new Color(0, 1, 1, 1);

        int width = shape.width * size;
        int height = shape.height * size;

        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);

        for (int x = 0; x < width; x ++) {
            float ratio = x / (float) width;
            Color top = Util.interpolateColor(topLeft, topRight, ratio);
            Color bottom = Util.interpolateColor(bottomLeft, bottomRight, ratio);

            for (int y = 0; y < height; y++) {
                Color color = Util.interpolateColor(top, bottom, y / (float) height);

                pixmap.setColor(color);
                pixmap.drawPixel(x, y);
            }
        }
        texture = new Texture(pixmap);
        pixmap.dispose();
    }

    public void deleteRandomLines() {
        if (field == null)
            throw new IllegalArgumentException("deleteRandomLines: Field has not been initialized!");

        int minLines = 1;
        int maxLines = 6;
        int minBit = 0;
        int maxBit = 7;

        Random rand = new Random(seed);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                int linesNumDelete = rand.nextInt((maxLines - minLines) + 1) + minLines;

                for (int k = 0; k < linesNumDelete; k++) {
                    int bitToDelete = rand.nextInt((maxBit - minBit) + 1) + minBit;

                    boolean canDeleteSide = canDeleteSide(j, i, bitToDelete);
                    if (!canDeleteSide)
                        continue;

                    int sideToDelete = (int) Math.pow(2, bitToDelete);

                    int neighbourBitToDelete = (int) (Math.log(Sides.oppositeSide.get(sideToDelete))/Math.log(2));
                    int neighbourColumn = (int) (j + Sides.oppositeCells.get(sideToDelete).x);
                    int neighbourRow = (int) (i + Sides.oppositeCells.get(sideToDelete).y);

                    boolean canDeleteNeighbour = canDeleteSide(neighbourColumn, neighbourRow, neighbourBitToDelete);

                    if (!canDeleteNeighbour)
                        continue;

                    deleteSide(j, i, bitToDelete);
                    deleteSide(neighbourColumn, neighbourRow, neighbourBitToDelete);
                }
            }
        }
    }

    public void applyShapeMask() {
        if (field == null)
            throw new IllegalArgumentException("applyShapeMask: Field has not been initialized!");

        if (shape == null)
            throw new IllegalArgumentException("applyShapeMask: Shape has not been initialized!");

        if (mainFigure == null)
            throw new IllegalArgumentException("applyShapeMask: Figure has not been initialized!");

        isMaskApplied = true;

        for (int i = 0; i < shape.height; i++) {
            for (int j = 0; j < shape.width; j++) {
                if (i < 0 || i >= field.length || j < 0 || j >= field[0].length)
                    continue;

                mainFigure[i][j] = field[i][j] & shape.shape[i][j];
                mainFigure[i][j] = mainFigure[i][j] | shape.border[i][j];
            }
        }
    }

    public void divideOnFigures() {
        if (field == null)
            throw new IllegalArgumentException("divideOnFigures: Field has not been initialized!");

        if (mainFigure == null)
            throw new IllegalArgumentException("divideOnFigures: Figure has not been initialized!");

        if (!isMaskApplied)
            throw new IllegalStateException("divideOnFigures: Mask is not applied yet!");

        for (int i = 0; i < mainFigure.length; i++) {
            for (int j = 0; j < mainFigure[0].length; j++) {
                getFigure(j, i);
            }
        }
        createIndexMask();
        isPolygonDrawing = true;
    }

    // --- Methods ---

    private boolean canDeleteSide(int column, int row, int bitToDelete) {
        if (row < 0 || row >= field.length || column < 0 || column >= field[0].length)
            return true;

        int value = field[row][column];

        int tmpValue = deleteBit(value, bitToDelete);
        return hasTwoOrMoreSides(tmpValue);
    }

    private void deleteSide(int column, int row, int bitToDelete) {
        if (row < 0 || row >= field.length || column < 0 || column >= field[0].length)
            return;

        int value = field[row][column];
        field[row][column] = deleteBit(value, bitToDelete);
    }

    private int deleteBit(int value, int bitToDelete) {
        return value & ~(1 << bitToDelete);
    }

    private boolean hasTwoOrMoreSides(int value) {
        int count = getSidesCount(value);
        return count >= 2;
    }

    private int getSidesCount(int value) {
        int count = 0;
        for (int side : Sides.sidesList) {
            if (hasSide(value, side))
                count++;
        }
        return count;
    }

    private void getFigure(int column, int row) {
        for (int side : Sides.sidesList) {
            if (hasSide(mainFigure[row][column], side)) {

                Figure figure = new Figure();
                figure.setMatrix(new int[mainFigure.length][mainFigure[0].length]);

                figure.addSideToCell(column, row, side);
                figure.addVertex(getCellCenterCoordinates(column, row));
                figure.addVertex(getCellSideCoordinates(column, row, side));

                findCorrespondingSide(figure, column, row, side);

                if (addFigureToList(figure)) {
                    figure.iniFigureParameters();
                    createTexture(figure);
                }
            }
        }
    }

    private boolean findCorrespondingSide(Figure figure, int column, int row, int side) {
        for (int i = 0; i < Sides.correspondingCells.get(side).length; i ++) {
            int nextColumn = column + (int) Sides.correspondingCells.get(side)[i].x;
            int nextRow = row + (int) Sides.correspondingCells.get(side)[i].y;
            if (nextRow < 0 || nextRow >= field.length || nextColumn < 0 || nextColumn >= field[0].length)
                continue;

            int correspondSide = Sides.correspondingSides.get(side)[i];
            if (hasSide(mainFigure[nextRow][nextColumn], correspondSide)) {
                figure.addSideToCell(nextColumn, nextRow, correspondSide);
                figure.addVertex(getCellCenterCoordinates(nextColumn, nextRow));

                int nextSideIndex = (int) (Math.log(correspondSide)/Math.log(2));
                findNextSide(figure, nextColumn, nextRow, nextSideIndex);
                return true;
            }
        }
        return false;
    }

    private void findNextSide(Figure figure, int column, int row, int sideIndex) {
        int currentSideIndex = sideIndex - 1;
        if (currentSideIndex == -1) currentSideIndex = 7;
        boolean allSidesChecked = false;

        while (!allSidesChecked){
            int side = Sides.sidesList[currentSideIndex];
            if (hasSide(mainFigure[row][column], side)) {

                if (hasSide(figure.getMatrix()[row][column], side))
                    return;

                figure.addSideToCell(column, row, side);
                figure.addVertex(getCellSideCoordinates(column, row, side));

                if (findCorrespondingSide(figure, column, row, side)) return;
            }

            currentSideIndex--;
            if (currentSideIndex == -1) currentSideIndex = 7;
            if (currentSideIndex == sideIndex)
                allSidesChecked = true;
        }
    }

    private boolean hasSide(int cellValue, int side) {
        int c = cellValue & side;
        return c == side;
    }

    private boolean addFigureToList(Figure figure) {
        for (Figure figureFromList : figureList) {
            int[][] matrix = figureFromList.getMatrix();
            if (Arrays.deepEquals(figure.getMatrix(), matrix))
                return false;
        }

        if (Arrays.deepEquals(figure.getMatrix(), shape.border)) {
            return false;
        }

        figureList.add(figure);
        return true;
    }

    private boolean findNeighborVertexes(Array<Body> bodies, Body body, Integer vertexIndex, final Vector2 vertex) {
        final float offset = 1.3f;
        Array<Vector2> vertexRectangle = new Array<Vector2>() {{
            add(new Vector2(vertex.x - offset, vertex.y - offset));
            add(new Vector2(vertex.x + offset, vertex.y - offset));
            add(new Vector2(vertex.x + offset, vertex.y + offset));
            add(new Vector2(vertex.x - offset, vertex.y + offset));
        }};

        for (int j = 0; j < bodies.size; j++) {
            Body bodyToCompare = bodies.get(j);
            if (!bodyToCompare.equals(body))  {
                Figure figureToCompare = (Figure) bodyToCompare.getUserData();
                if (figureToCompare == null)
                    continue;

                Array<Vector2> vertices1 = Util.moveVector2Array(figureToCompare.getVerticesBox2D(), bodyToCompare.getPosition());
                for (int k = 0; k < vertices1.size; k++) {
                    if (Intersector.isPointInPolygon(vertexRectangle, vertices1.get(k))) {
                        if (vertexIndex.equals(figureToCompare.getIndexes().get(k))) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private void createTexture(Figure figure) {
        float u2 =  (figure.getPosition().x - offsetX + figure.getWidth()) / texture.getWidth();
        float v2 =  (figure.getPosition().y - offsetY) / texture.getHeight() ;
        float u1 =  (figure.getPosition().x - offsetX) /texture.getWidth();
        float v1 =  (figure.getPosition().y - offsetY + figure.getHeight()) / texture.getHeight();

        TextureRegion textureRegion = new TextureRegion(texture,
                u1, v1,
                u2, v2);

        EarClippingTriangulator earClippingTriangulator = new EarClippingTriangulator();
        ShortArray triangles = earClippingTriangulator.computeTriangles(Util.getFloatArray(figure.getVerticesSprite()));
        PolygonRegion polygonRegion = new PolygonRegion(textureRegion, Util.getFloatArray(figure.getVerticesSprite()),
                triangles.items);
        PolygonSprite polygonSprite = new PolygonSprite(polygonRegion);
        figure.setPolygonSprite(polygonSprite);
    }

    private void createIndexMask() {
        ObjectMap<Integer, Integer> tmp = new ObjectMap<Integer, Integer>();
        for (Figure figure : figureList) {
            for (int i : figure.getIndexes()) {
                if (tmp.containsKey(i)) tmp.put(i, tmp.get(i) + 1);
                else tmp.put(i, 1);
            }
        }
        for (Figure figure : figureList) {
            for (int i : figure.getIndexes()) {
                figure.addToIndexMask(tmp.get(i) != 1);
            }
        }
    }

    private Vector2 getCellCenterCoordinates(int column, int row) {
        return new Vector2(column * size + halfSize + offsetX,
                row * size + halfSize + offsetY);
    }

    private Vector2 getCellSideCoordinates(int column, int row, int side) {
        return new Vector2(column * size + halfSize + Sides.coordinatesStep.get(side).x * size + offsetX,
                row * size + halfSize + Sides.coordinatesStep.get(side).y * size + offsetY);
    }

    // --- Debug methods ---

    public void switchDrawShape() {
        isShapeDrawing = !isShapeDrawing;
    }

    public void decrementIndex() {
        if (figureList.size == 0)
            return;

        if (polygonIndex > 0)
            polygonIndex--;
    }

    public void incrementIndex() {
        if (figureList.size == 0)
            return;

        if (polygonIndex < figureList.size - 1)
            polygonIndex++;
    }

    // --- Draw debug ---

    /**
     *
     * @param batch begin will not be called
     */
    public void drawTexture(SpriteBatch batch) {
        if (texture == null) {
            Gdx.app.log("Error", "Texture is not initialized");
            return;
        }
        batch.draw(texture, offsetX, offsetY);
    }

    /**
     * @param shapeRenderer begin(Line) will be called
     */
    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        drawDebugNet(shapeRenderer);

        if (isMaskApplied) {
            shapeRenderer.setColor(0, 1, 0, 1);
            drawMatrix(shapeRenderer, mainFigure);
            if (isPolygonDrawing) {
                int[][] matrix = figureList.get(polygonIndex).getMatrix();
                shapeRenderer.setColor(1, 1, 1, 1);
                drawMatrix(shapeRenderer, matrix);
            }
        } else {
            shapeRenderer.setColor(0, 1, 0, 1);
            drawMatrix(shapeRenderer, field);
        }

        if (isShapeDrawing) {
            shapeRenderer.setColor(0, 0, 1, 1);
            drawMatrix(shapeRenderer, shape.shape);
        }

        shapeRenderer.end();
    }

    private void drawDebugNet(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(1, 0, 0, 1);
        for (int i = 0; i < shape.height; i++) {
            for (int j = 0; j < shape.width; j++) {
                shapeRenderer.rect((float) (j * size) + offsetX, (float) (i * size) + offsetY, size, size);
            }
        }
    }

    private void drawMatrix(ShapeRenderer shapeRenderer, int[][] matrix) {
        if (matrix == null) {
            Gdx.app.log("Error", "Matrix is not initialized");
            return;
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (int side : Sides.sidesList)
                    if (hasSide(matrix[i][j], side))
                        shapeRenderer.line(getCellCenterCoordinates(j, i), getCellSideCoordinates(j, i, side));
            }
        }
    }
    // --------------------
}
