package com.vdroog1.figurePuzzle.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.FrictionJoint;
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.vdroog1.figurePuzzle.Constants;
import com.vdroog1.figurePuzzle.Util;
import com.vdroog1.figurePuzzle.model.Figure;
import com.vdroog1.figurePuzzle.screen.GameScreen;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by kettricken on 13.03.2015.
 */
public class BodyProcessor {

    private static final int MIN_DISTANCE = 3;
    private static final float COEFFICIENT = 10;
    private static final float SHUFFLING_COEFFICIENT = 0.8f;
    private static final float BODY_ROTATION_SPEED = 0.5f;
    private static final int SHUFFLING_ITERATIONS = 2;

    private static final short CATEGORY_OBJECT = 0x0001;
    private static final short CATEGORY_GROUND = 0x0002;
    private static final short MASK_OBJECT = CATEGORY_OBJECT;
    private static final short MASK_GROUND = 0;

    private World world;
    private Body groundBody;

    private Array<Body> bodies = new Array<Body>();
    private ObjectSet<Body> movingBodies = new ObjectSet<Body>();
    private Array<Body> bodiesToDelete = new Array<Body>();
    private Array<MouseJoint> mouseJointsToDelete = new Array<MouseJoint>();
    private Body clickedBody;

    private float maxBodyDistance = 0;
    private float desiredWorldSize = 0;

    private float speedCoefficient = COEFFICIENT;
    private int shufflingIterations = 0;

    private GameScreen gameScreen;

    private Body groupBody;

    public BodyProcessor(World world, GameScreen gameScreen) {
        this.world = world;
        this.gameScreen = gameScreen;
        world.setContactListener(new CustomContactListener());
    }

    public Array<Body> getBodies() {
        return bodies;
    }

    public Body getClickedBody() {
        return clickedBody;
    }

    public float getDesiredWorldSize() {
        return desiredWorldSize;
    }

    public int getMovingBodiesCount() {
        return movingBodies.size;
    }

    public void createWalls() {
        float HALF_WALL_HEIGHT = 2.5f;

        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(Constants.WORLD_WIDTH_M / 2, HALF_WALL_HEIGHT));

        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(Constants.WORLD_WIDTH_M / 2, HALF_WALL_HEIGHT);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.filter.categoryBits = CATEGORY_GROUND;
        fixtureDef.filter.maskBits = MASK_GROUND;
        fixtureDef.shape = groundBox;
        fixtureDef.density = 0f;

        groundBody = world.createBody(groundBodyDef);
        groundBody.createFixture(fixtureDef);

        groundBox.dispose();
    }

    public void createBody(Figure figure) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(figure.getCenterBox2D().x, figure.getCenterBox2D().y);

        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        for (Polygon polygon : figure.getConvexPolygons()) {
            polygonShape.set(polygon.getVertices());

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            fixtureDef.restitution = 0f;
            fixtureDef.density = 0f;
            fixtureDef.friction = 0f;
            fixtureDef.filter.categoryBits = CATEGORY_OBJECT;
            fixtureDef.filter.maskBits = MASK_OBJECT;

            body.createFixture(fixtureDef);
        }
        polygonShape.dispose();

        body.setUserData(figure);
        bodies.add(body);
    }

    public void resetBodies() {
        Gdx.app.log("Test", "resetBodies");
        for (Body body : bodies){
            body.setAngularVelocity(0);
            body.setLinearVelocity(0, 0);

            Figure figure = ((Figure) body.getUserData());
            if (figure == null)
                continue;

            if (figure.getMouseJoint() != null) {
                mouseJointsToDelete.add(figure.getMouseJoint());
                figure.getMouseJoint().setTarget(body.getPosition());
            }

            body.setTransform(figure.getCenterBox2D().x, figure.getCenterBox2D().y, 0);
            figure.reset();
        }

        movingBodies.clear();
        bodiesToDelete.clear();
        clickedBody = null;
    }

    public void randomize() {
        for (Body body : bodies) {
            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            for ( Fixture fixture : body.getFixtureList()) {
                Filter filter = fixture.getFilterData();
                filter.categoryBits = CATEGORY_GROUND;
                filter.maskBits = MASK_GROUND;
                fixture.setFilterData(filter);
            }

            if (body == clickedBody)
                continue;

            float x = Util.randomInRange(0, Constants.WORLD_WIDTH_M);
            float y = Util.randomInRange(0, Constants.WORLD_HEIGHT_M);

            figure.setDestinationPoint(x, y);

            float distanceBetweenCenters = (float) Math.sqrt(Math.pow(body.getPosition().x - x, 2)
                    + Math.pow(body.getPosition().y - y, 2));
            float desiredDistance = distanceBetweenCenters;

            figure.setDesiredDistance(desiredDistance);
        }
    }

    public boolean tapBody(Vector3 touchPoint) {
        for (int j = 0; j < bodies.size; j++) {
            Body body = bodies.get(j);

            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            Polygon[] polygons = figure.getConvexPolygons();

            for (Polygon polygon : polygons) {
                polygon.setRotation(Util.radiansToDegrees(body.getAngle()));
                polygon.setPosition(body.getPosition().x, body.getPosition().y);

                if (Intersector.isPointInPolygon(polygon.getTransformedVertices(),
                        0, polygon.getVertices().length,
                        touchPoint.x, touchPoint.y)) {

                    clickedBody = body;
                    float maxBodySize = setBodiesMoving();

                    int padding = 10;
                    desiredWorldSize = maxBodyDistance * 2 + maxBodySize + padding;
                    return true;
                }
            }
        }
        return false;
    }

    private void tapBody(Body body) {
        clickedBody = body;
        float maxBodySize = setBodiesMoving();

        int padding = 10;
        desiredWorldSize = maxBodyDistance * 2 + maxBodySize + padding;
    }

    private float setBodiesMoving() {
        if (clickedBody == null)
            return 0;

        float maxSize = 0;
        for (Body body : bodies) {
            if (body.equals(clickedBody))
                continue;

            if (body.getType() != BodyDef.BodyType.DynamicBody)
                continue;

            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            Figure clickedFigure = (Figure) clickedBody.getUserData();
            if (clickedFigure == null)
                continue;

            Vector2 currentPosition = body.getPosition();
            Vector2 clickedPosition = clickedBody.getPosition();

            MouseJointDef def = new MouseJointDef();
            def.bodyA = groundBody;
            def.bodyB = body;
            def.collideConnected = true;
            def.target.set(currentPosition.x, currentPosition.y);
            def.maxForce = 10000.0f;

            MouseJoint mouseJoint = (MouseJoint) world.createJoint(def);
            Gdx.app.log("Test", "createJoint");
            body.setAwake(true);

            float distanceBetweenCenters = (float) Math.sqrt(Math.pow(currentPosition.x - clickedPosition.x, 2)
                    + Math.pow(currentPosition.y - clickedPosition.y, 2));
            float speed = distanceBetweenCenters / speedCoefficient;
            float desiredDistance = MIN_DISTANCE * distanceBetweenCenters;

            float destinationX = currentPosition.x - desiredDistance / distanceBetweenCenters * (clickedPosition.x - currentPosition.x);
            float destinationY = currentPosition.y - desiredDistance / distanceBetweenCenters * (clickedPosition.y - currentPosition.y);

            figure.setDestinationPoint(destinationX, destinationY);
            figure.setMouseJoint(mouseJoint);
            figure.setSpeed(speed);
            figure.setDesiredDistance(desiredDistance);

            movingBodies.add(body);

            if (desiredDistance > maxBodyDistance) {
                maxBodyDistance = desiredDistance;
            }

            float s = Math.max(figure.getWidth(), figure.getHeight());
            if (s > maxSize)
                maxSize = s;
        }

        return maxSize;
    }

    public int updateBodies(float delta, int rotationDirection) {
        int stoppedBodiesCount = translateBodies();
        rotateBody(rotationDirection);
        deleteStoppedBodies();
        return  stoppedBodiesCount;
    }

    private int translateBodies() {
        float currentBodyDistance = 0;
        int stoppedBodiesCount = 0;
        for (Body body : bodies) {
            if (!movingBodies.contains(body)) {
                body.setLinearVelocity(0, 0);
            } else {
                Figure figure = (Figure) body.getUserData();

                if (figure == null) continue;

                if (figure.getMouseJoint() == null) continue;

                Vector2 currentPosition = body.getPosition();
                Vector2 clickedPosition = clickedBody.getPosition();

                if (gameScreen.isExpanding() || gameScreen.isRandomizing()) {
                    float desiredDistance = figure.getDesiredDistance();
                    float realDistance = (float) Math.sqrt(Math.pow(currentPosition.x - clickedPosition.x, 2)
                            + Math.pow(currentPosition.y - clickedPosition.y, 2));

                    if (realDistance < desiredDistance) {
                        Vector2 destination = figure.getDestination();

                        float diff = figure.move(currentPosition, destination, figure.getSpeed());

                        if (diff > currentBodyDistance) {
                            currentBodyDistance = diff;
                        }
                        if (diff <= 0.5)
                            stoppedBodiesCount++;
                    } else if (realDistance >= desiredDistance) {
                        stoppedBodiesCount++;
                    }
                } else if(gameScreen.isUniting()) {
                    float diff = figure.move(currentPosition, clickedPosition, figure.getSpeed());

                    if (diff <= 0.5){
                        bodiesToDelete.add(body);
                    }
                }
            }
        }
        return stoppedBodiesCount;
    }

    private void rotateBody(int rotationDirection) {
        if (clickedBody == null)
            return;
        if (gameScreen.isRotating()) {
            clickedBody.setAngularVelocity(BODY_ROTATION_SPEED * rotationDirection);
            rotateJointBodies(clickedBody, BODY_ROTATION_SPEED * rotationDirection);
        } else {
            clickedBody.setAngularVelocity(0);
            rotateJointBodies(clickedBody, 0);
        }
    }

    private void rotateJointBodies(Body clickedBody, float angularVelocity) {
        Figure figure = (Figure) clickedBody.getUserData();
        for (Body body : figure.getJointBodies()) {
            body.setAngularVelocity(angularVelocity);
        }
    }

    private void deleteStoppedBodies() {
        Iterator<Body> bodyIterator = bodiesToDelete.iterator();
        while (bodyIterator.hasNext()){
            Body body = bodyIterator.next();
            if (movingBodies.contains(body)) {
                Figure figure = (Figure) body.getUserData();

                if (figure.getMouseJoint() != null) {
                    Gdx.app.log("Test", "destroy joint");
                    world.destroyJoint(figure.getMouseJoint());
                }
                movingBodies.remove(body);
            }
            body.setLinearVelocity(0, 0);
            bodyIterator.remove();
        }
    }

    public boolean shuffle() {

        shufflingIterations++;

        if (shufflingIterations > SHUFFLING_ITERATIONS) {
            speedCoefficient = COEFFICIENT;
            shufflingIterations = 0;
            return false;
        }

        speedCoefficient = SHUFFLING_COEFFICIENT;

        int max = bodies.size;
        int min = 0;
        Random random = new Random();
        int index = random.nextInt(max - min);
        Body body = bodies.get(index);

        tapBody(body);
        return true;
    }

    public void restoreMask() {
        for (Body body : bodies) {
            for (Fixture fixture : body.getFixtureList()) {
                Filter filter = fixture.getFilterData();
                filter.categoryBits = CATEGORY_OBJECT;
                filter.maskBits = MASK_OBJECT;
                fixture.setFilterData(filter);
            }
        }
    }

    public void cleanMouseJoints() {
        Iterator<MouseJoint> mouseJointIterator = mouseJointsToDelete.iterator();
        while (mouseJointIterator.hasNext()) {
            MouseJoint mouseJoint = mouseJointIterator.next();
            Gdx.app.log("Test", "cleanMouseJoints destroy joint mouseJoint " + mouseJoint.isActive());
           // world.destroyJoint(mouseJoint);
            mouseJointIterator.remove();
        }
    }

    public void groupBodies(Vector3 touchPoint) {
        for (int j = 0; j < bodies.size; j++) {
            Body body = bodies.get(j);

            Figure figure = (Figure) body.getUserData();
            if (figure == null)
                continue;

            Polygon[] polygons = figure.getConvexPolygons();

            for (Polygon polygon : polygons) {
                polygon.setRotation(Util.radiansToDegrees(body.getAngle()));
                polygon.setPosition(body.getPosition().x, body.getPosition().y);

                if (Intersector.isPointInPolygon(polygon.getTransformedVertices(),
                        0, polygon.getVertices().length,
                        touchPoint.x, touchPoint.y)) {

                    if (groupBody == null) {
                        groupBody = body;
                        return;
                    } else {

                        DistanceJointDef def = new DistanceJointDef();
                        def.initialize(groupBody, body, groupBody.getPosition(), body.getPosition());
                        def.bodyB = body;
                        def.collideConnected = true;

                        DistanceJoint distanceJoint = (DistanceJoint) world.createJoint(def);

                        figure.addJointBody(groupBody);
                        Figure groupFigure = (Figure) groupBody.getUserData();
                        groupFigure.addJointBody(body);

                        groupBody = null;
                    }
                }
            }
        }
    }

    private class CustomContactListener implements ContactListener {

        @Override
        public void beginContact(Contact contact) {

            if (gameScreen.isShuffling() && gameScreen.isExpanding()){
                return;
            }

            Body bodyA = contact.getFixtureA().getBody();
            Body bodyB = contact.getFixtureB().getBody();

            if (!movingBodies.contains(bodyB)) {
                bodiesToDelete.add(bodyA);
                checkContacts(bodyA);
            }

            if (!movingBodies.contains(bodyA) ) {
                bodiesToDelete.add(bodyB);
                checkContacts(bodyB);
            }

            contact.setEnabled(false);
        }

        @Override
        public void endContact(Contact contact) { }

        @Override
        public void preSolve(Contact contact, Manifold manifold) { }

        @Override
        public void postSolve(Contact contact, ContactImpulse contactImpulse) { }

        private void checkContacts(Body body) {
            Array<Contact> contacts = world.getContactList();
            for (Contact contact : contacts) {

                Body bodyA = contact.getFixtureA().getBody();
                Body bodyB = contact.getFixtureB().getBody();

                if (bodyA.equals(body) && movingBodies.contains(bodyB) && !bodiesToDelete.contains(bodyB, true)) {
                    if (contact.isTouching())
                        bodiesToDelete.add(bodyB);
                }

                if (bodyB.equals(body) && movingBodies.contains(bodyA) && !bodiesToDelete.contains(bodyA, true)) {
                    if (contact.isTouching())
                        bodiesToDelete.add(bodyA);
                }
            }
        }
    }

}
