package com.vdroog1.figurePuzzle.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class DirectionGestureDetector extends GestureDetector {

    public  interface DirectionListener{
        void clockwise();
        void counterClockwise();
    }

    private Vector3 touchPoint = new Vector3();
    private DirectionGestureListener gestureListener;

    public DirectionGestureDetector(DirectionGestureListener directionListener) {
        super(directionListener);
        this.gestureListener = directionListener;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchPoint.set(screenX, screenY, 0);
        gestureListener.setTouchPoint(touchPoint.x, touchPoint.y);
        return false;
    }

    public static class  DirectionGestureListener extends GestureAdapter {

        DirectionListener directionListener;
        Vector2 touchPoint = new Vector2();

        public DirectionGestureListener(DirectionListener directionListener){
            this.directionListener = directionListener;
        }

        public void setTouchPoint(float x, float y) {
            touchPoint.set(x, y);
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            if(Math.abs(deltaX )> Math.abs(deltaY)){
                if(deltaX > 0){
                    if (touchPoint.y <= Gdx.graphics.getHeight() / 2) {
                        directionListener.clockwise();
                    } else {
                        directionListener.counterClockwise();
                    }
                } else {
                    if (touchPoint.y <= Gdx.graphics.getHeight() / 2) {
                        directionListener.counterClockwise();
                    } else {
                        directionListener.clockwise();
                    }

                }
            }else{
                if(deltaY > 0){
                    if (touchPoint.x <= Gdx.graphics.getWidth() / 2) {
                        directionListener.counterClockwise();
                    } else {
                        directionListener.clockwise();
                    }
                } else {
                    if (touchPoint.x <= Gdx.graphics.getWidth() / 2) {
                        directionListener.clockwise();
                    } else {
                        directionListener.counterClockwise();
                    }
                }
            }
            return super.pan(x, y, deltaX, deltaY);
        }
    }
}
