package com.vdroog1.figurePuzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by kettricken on 08.03.2015.
 */
public class Util {

    private void printIntegerArray(Array<Integer> indexes) {
        String verticesStr = "\n[ ";
        for (Integer index : indexes) {
            verticesStr += index + ", ";
        }
        verticesStr += " ]";
        Gdx.app.log("printIntegerArray", verticesStr);
    }

    public static void printVector2Array(Array<Vector2> vertices) {
        String verticesStr = "\n[ ";
        for (Vector2 vertex : vertices) {
            verticesStr += "(" + vertex.x + "," + vertex.y + "),";
        }
        verticesStr += " ]";
        Gdx.app.log("printVector2Array", verticesStr);
    }

    public static void printBooleanArray(Array<Boolean> array) {
        String string = "[";
        for (Boolean b : array) {
            string += b + ",";
        }
        string += "]";
        Gdx.app.log("printBooleanArray", string);
    }

    public static float[] moveFloatArray(Polygon polygon, Vector2 position) {
        float[] vertices = new float[polygon.getVertices().length];
        for (int i = 0; i < polygon.getVertices().length; i ++) {
            if (i % 2 == 0) {
                vertices[i] = polygon.getVertices()[i] + position.x;
            } else {
                vertices[i] = polygon.getVertices()[i] + position.y;
            }
        }
        return vertices;
    }

    public static float[] getFloatArray(Array<Vector2> vector2Array) {
        float[] array = new float[vector2Array.size * 2];
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0)
                array[i] = vector2Array.get((int) (i / 2)).x;
            else
                array[i] = vector2Array.get((int) (i / 2)).y;
        }
        return array;
    }

    public static Array<Vector2> moveVector2Array(Array<Vector2> vertices, Vector2 position) {
        Array<Vector2>  result = new Array<Vector2>();
        for (Vector2 vector2 : vertices) {
            result.add(new Vector2(vector2.x + position.x, vector2.y + position.y));
        }
        return result;
    }

    public static Color interpolateColor(Color startColor, Color endColor, float ratio) {
        Color color = new Color();
        color.r = (1 - ratio) * startColor.r + ratio * endColor.r;
        color.g = (1 - ratio) * startColor.g + ratio * endColor.g;
        color.b = (1 - ratio) * startColor.b + ratio * endColor.b;
        color.a = 1;

        return color;
    }

    public static float radiansToDegrees(float radians) {
        float degrees = radians * MathUtils.radiansToDegrees;

        while(degrees <= 0){
            degrees += 360;
        }

        while(degrees > 360){
            degrees -= 360;
        }
        return degrees;
    }

    protected static Random random = new Random();
    public static float randomInRange(float min, float max) {
        float range = max - min;
        float scaled = random.nextFloat() * range;
        float shifted = scaled + min;
        return shifted;
    }
}
