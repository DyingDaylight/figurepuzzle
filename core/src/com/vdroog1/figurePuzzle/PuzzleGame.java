package com.vdroog1.figurePuzzle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.vdroog1.figurePuzzle.screen.GameScreen;

public class PuzzleGame extends Game {

    SpriteBatch batch;

    @Override
    public void create () {
        batch = new SpriteBatch();

        setScreen(new GameScreen(this));
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render();
    }

    public SpriteBatch getBatch() {
        return batch;
    }
}
