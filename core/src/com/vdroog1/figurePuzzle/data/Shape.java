package com.vdroog1.figurePuzzle.data;

/**
 * Created by kettricken on 01.03.2015.
 */
public enum Shape {

    SQUARE (
        new int [][] {
            new int[] {28, 124, 124, 124, 112},
            new int[] {31, 255, 255, 255, 241},
            new int[] {31, 255, 255, 255, 241},
            new int[] {31, 255, 255, 255, 241},
            new int[] {7, 199, 199, 199, 193},
    },
        new int [][] {
            new int[] {20, 68, 68, 68, 80},
            new int[] {17, 0, 0, 0, 17},
            new int[] {17, 0, 0, 0, 17},
            new int[] {17, 0, 0, 0, 17},
            new int[] {5, 68, 68, 68, 65},
    });

    public int[][] shape;
    public int[][] border;

    public int width;
    public int height;

    Shape(int[][] shape, int[][] border) {
        this.shape = shape;
        this.border = border;

        width = shape[0].length;
        height = shape.length;
    }
}
