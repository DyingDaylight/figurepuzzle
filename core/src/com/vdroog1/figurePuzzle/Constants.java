package com.vdroog1.figurePuzzle;

/**
 * Created by kettricken on 08.02.2015.
 */
public class Constants {

    public static final float TIME_STEP = 1/60f;
    public static final int VELOCITY_ITERATIONS = 6;
    public static final int POSITION_ITERATIONS = 2;

    public static final int WORLD_WIDTH_M = 100;
    public static final int WORLD_HEIGHT_M = 60;
}
